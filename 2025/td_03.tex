\documentclass[a4paper,12pt]{letter}
\usepackage{amsmath,ifthen}
\usepackage{ucs} 
\usepackage{bm}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{float}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{latexsym}
\usepackage[utf8x]{inputenc}
\usepackage{bbm}
\usepackage{mathtools}

\newcommand{\boldgreek}[1]{\mbox{\boldmath$#1$\unboldmath}}
\newcommand{\bxi}{\boldgreek{\xi}}
\newcommand{\br}{{\bf r}}
\newcommand{\bq}{{\bf q}}
\newcommand{\bp}{{\bf p}}
\newcommand{\bx}{{\bf x}}
\newcommand{\be}{{\bf e}}
\newcommand{\bE}{{\bf E}}
\newcommand{\bu}{{\bf u}}
\newcommand{\bv}{{\bf v}}
\newcommand{\ba}{{\bf a}}
\newcommand{\bB}{{\bf B}}
\newcommand{\bfu}{{\bm f}}
\newcommand{\bF}{{\bm F}}

\pagestyle{empty}
\setlength{\textwidth}{16cm}
\setlength{\textheight}{26cm}
\setlength{\parindent}{0mm}

\setlength{\topmargin}{-1cm}
\setlength{\headheight}{0mm}
\setlength{\headsep}{0mm}

\setlength{\marginparwidth}{0mm}
\setlength{\marginparpush}{0mm}
\setlength{\marginparsep}{0mm}
\setlength{\oddsidemargin}{0mm}

\begin{document}

{\bf TD 3} \hfill {\bf MAUCA 2024/2025}\\[5mm]

\begin{center}
{\bf \large General Physics - Plasmas}\vspace*{2mm}
\hrule
\end{center}

\vspace{0.5cm}

{\bf 1) Grad B drift }

We will estimate the drift from a non-uniform magnetic field
$\bB$. Suppose that $\bB = \bB(y) = B_z(y)\be_z$. Drift means drift of
the guiding center. Does the non-uniform magnetic field induce an
average force over one particle orbit?
\begin{itemize}
\item Compute the Lorentz force on the particle.
\item What is the orbit of a particle when its guiding center is at
  $(0,0)$?
\item Approximate $\bB$ by a first-order Taylor expansion.
\item Compute the Lorentz force in this approximation using the
  unperturbed orbit.
\item Average this force over one orbit.
\item Show that the general drift force is
  \begin{equation*}
    \bv_D = - \frac{1}{2}r_L^2\omega_c \frac{\bB \wedge \nabla \bB}{B^2}
  \end{equation*}
  
\end{itemize}

{\bf 2) Langmuir waves with the Vlasov equation }

We want to find one of the simplest plasma waves, called Langmuir
wave, from the Vlasov equation. The calculation will a bit lengthy and
will involve a lot of assumption and simplifications. This exercise is
worth doing it because it provides a concrete example of a solution of
a kinetic equation. What we are striving for is this simple dispersion
relation (relating $k$ and $\omega$ of a plane wave)
\begin{equation*}
  \omega^2=\omega_e^2 + 3k^2 v_{th_e}^2,
\end{equation*}
$v_{th_e}=kT_e/m_e$ being the thermal electron velocity and $\omega_e$
the plasma frequency.

Let us consider a plasma composed of electrons with charge $q_e=-e$
and mass $m_e$ and ions with charge $q_i=e$ and mass $m_i$. We would
like to search for longitudinal waves so that we assume a simple
electric field
\begin{equation*}
  \bE = E_x \be_x = A_E e^{i(kx-\omega t)}\be_x
\end{equation*}
\begin{itemize}
  \item Verify first (using Maxwell's equations) that no magnetic
    field arises.
\end{itemize}

We want to look for solutions of the Vlasov equations
\begin{equation*}
  \partial_t f_s + \bv \cdot \nabla_x f_s + \frac{q_s}{m_s} \bE\cdot
  \nabla_v f_s = 0,
\end{equation*}
where $f_s$ stands for $f_e$ and $f_i$ (electrons and ions). More
specifically, we look for wave-like perturbations 
\begin{equation*}
  f_s^1=A_{f_s}e^{i(kx-\omega t)}
\end{equation*}
perturbing an equilibrium $f_s^0$ given by a Maxwell-Boltzmann
distribution. We assume a small perturbation controlled by a small
parameter $\epsilon$ so that
\begin{equation}
  \label{eq:ansatz_f_s}
  f_s = f_s^0 + \epsilon f_s^1.
\end{equation}
\begin{itemize}
  \item Plug (\ref{eq:ansatz_f_s}) in the Vlasov equation and to
    derive a zero-order and a first-order equation in
    $\epsilon$. Why is $E_x$ linearly depending on $f_s^1$? Use
    Maxwell's ($\nabla \cdot \bE = ...$) equations.
  \item Show that
    \begin{equation}
      \label{eq:A_f_s}
      A_{f_s} = \frac{iq_s}{\epsilon m_s(k v_x - \omega)} A_E \partial_{v_x}f_s^0
    \end{equation}
    and
    \begin{equation*}
      f_s^1 = \frac{iq_s}{\epsilon m_s(k v_x - \omega)} E_x \partial_{v_x}f_s^0
    \end{equation*}
    Use Maxwell's ($\nabla \cdot \bE = ...$) equation and
    (\ref{eq:A_f_s}) to derive the dispersion relation
    \begin{equation*}
      1 + \frac{4\pi e^2}{m_e k^2}\int dv^3 \frac{1}{\omega/k - v_x} \left( \frac{m_e}{m_i} \partial_{v_x} f_i^0 + \partial_{v_x} f_e^0\right) = 0
    \end{equation*}
  \item Use the plasma frequency $\omega_e$ to bring it in the form
    \begin{align}
      \label{eq:dispersion_1}
      1 + \frac{\omega_e^2}{n_0 k^2}\int dv_x \frac{1}{\omega/k - v_x}\frac{dg(v_x)}{dv_x} = 0\\
      g(v_x) = \int dv_y dv_z \frac{m_e}{m_i} \partial_{v_x} f_i^0 + \partial_{v_x} f_e^0
    \end{align}
  \item Further assumptions and simplifications: 1) Use
    Maxwell-Boltzmann distributions for $f_s^0.$ 2) Assume $T_e = T_i$
    and $m_i \gg m_e$ to find
    \begin{equation*}
      g(v_x) = \frac{1}{\sqrt{2\pi}v_{th_e}}e^{-\frac{v_x^2}{2v_{th_e}^2}},
    \end{equation*}
    where $v_{th_e}=kT_e/m_e$ is the thermal electron velocity.
  \item Now, we would like to integrate (\ref{eq:dispersion_1}). We
    need to handle the pole at $v_x=\omega/k$. For our considered
    waves we assume $\omega/k \gg v_x$ so that $dg/dv_x=0$ at the
    pole. Think about this. Assume $v_xk/\omega \ll 1$ and expand
    $\frac{1}{\omega/k - v_x}$ in a series:
    \begin{equation*}
      \frac{1}{\omega/k - v_x}=\frac{k}{\omega}\left(1 +
      \frac{v_xk}{\omega} + \left(\frac{v_xk}{\omega}\right)^2 +
      \left(\frac{v_xk}{\omega}\right)^3 + \cdots\right)
    \end{equation*}
  \item Do the integral in (\ref{eq:dispersion_1}) (maybe using
    Wolfram alpha) to find the dispersion relation
    \begin{equation*}
      1-\frac{\omega_e^2}{\omega^2} - \frac{3k^2v_{th_e}^2}{\omega^4}
      = 0
    \end{equation*}
  \item Last step: Derive from this the Langmuir wave dispersion
    relation under the assumption $\omega^2\gg k^2v_{th_e}^2$
    \begin{equation*}
      \omega^2=\omega_e^2 + 3k^2 v_{th_e}^2
    \end{equation*}
\end{itemize}
\end{document}

