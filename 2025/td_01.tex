\documentclass[a4paper,12pt]{letter}
\usepackage{amsmath,ifthen}
\usepackage{ucs} 
\usepackage{bm}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{float}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{latexsym}
\usepackage[utf8x]{inputenc}
\usepackage{bbm}
\usepackage{mathtools}

\newcommand{\boldgreek}[1]{\mbox{\boldmath$#1$\unboldmath}}
\newcommand{\bxi}{\boldgreek{\xi}}
\newcommand{\br}{{\bf r}}
\newcommand{\bq}{{\bf q}}
\newcommand{\bp}{{\bf p}}
\newcommand{\bx}{{\bf x}}
\newcommand{\be}{{\bf e}}
\newcommand{\bu}{{\bf u}}
\newcommand{\bv}{{\bf v}}
\newcommand{\ba}{{\bf a}}
\newcommand{\bB}{{\bf B}}
\newcommand{\bfu}{{\bm f}}
\newcommand{\bF}{{\bm F}}

\pagestyle{empty}
\setlength{\textwidth}{16cm}
\setlength{\textheight}{26cm}
\setlength{\parindent}{0mm}

\setlength{\topmargin}{-1cm}
\setlength{\headheight}{0mm}
\setlength{\headsep}{0mm}

\setlength{\marginparwidth}{0mm}
\setlength{\marginparpush}{0mm}
\setlength{\marginparsep}{0mm}
\setlength{\oddsidemargin}{0mm}

\begin{document}

{\bf TD 1} \hfill {\bf MAUCA 2024/2025}\\[5mm]

\begin{center}
{\bf \large General Physics - Plasmas}\vspace*{2mm}
\hrule
\end{center}

\vspace{0.5cm}

{\bf 1) The phase space $\Gamma$}

To get familiar with the phase space $\Gamma$ we consider the motion
of a harmonic oscillator with masse $m=1$ given by

\begin{equation*}
  \ddot q(t) + \omega^2 q(t) = 0
\end{equation*}

with $q(0) = q_0$ and $v=\dot q(0) = 0$.

\begin{enumerate}
\item Sketch the motion of the oscillator in diagrams $t-q$ and $t-v$.
\item Sketch the motion in phase space $\Gamma$ ($q,p$).
\end{enumerate}

Consider now a system of two freely moving particles with different
velocities given by

\begin{equation*}
  \ddot q_i(t)  = 0
\end{equation*}

\begin{enumerate}
\item Sketch their motion in phase space $\Gamma$.
\item What happens to their phase space trajectories during a
  collision?  Make a sketch.
\end{enumerate}

{\bf 2) Averaged Klimontovich equation}

In this exercise we want to average the Klimontovich equation

\begin{align*}
  \frac{\partial N(\bx, \bp, t)}{\partial t} + \frac{1}{m}\bp \cdot
  \nabla_{\bx} N(\bx, \bp,t ) + \bF(\bx, \bp) \cdot \nabla_{\bp}
  N(\bx, \bp,t) = 0.
\end{align*}

For this, decompose the distribution function $N=N(\bx,\bp,t)$
and the force $\bF=\bF(\bx, \bp)$ into a mean and a
fluctuating contribution:

\begin{equation*}
  N=\bar N + \delta N \quad \& \quad \bF = \bar \bF + \delta \bF
\end{equation*}

Insert this into the Klimontovich equation and average the whole
equation.


{\bf 3) Continuity equation in phase space $\Gamma$}

The collisionless Boltzmann equation
\begin{align*}
  \frac{\partial f(\bx, \bp, t)}{\partial t} + \frac{1}{m}\bp
  \cdot \nabla_{\bx} f(\bx, \bp,t ) + \bF(\bx, \bp) \cdot
  \nabla_{\bp} f(\bx, \bp,t) = 0
\end{align*}
can be derived on the phase space in the form of a continuity
equation. The main assumption (continuity) is that no particles are
created or destroyed.

Consider the (two-dimensional) phase space coordinate
$\boldsymbol{\mu}=(x,p)$. The starting point is the number of
particles $N$ in a phase space volume $V$:
\begin{equation*}
N_V(t) = \int_V f(\boldsymbol{\mu},t)\,{\rm d}V
\end{equation*}
We want to derive an equation for the temporal variation of $N_V$. Use
the assumption that $N_V$ can only be changed by entering and leaving
particles at the boundary of $V$.

Show that (assuming a static volume $V$)
\begin{equation*}
\frac{\partial}{\partial t}f(\boldsymbol{\mu},t)\,+\,
\boldsymbol{\nabla_\mu}\cdot(f(\boldsymbol{\mu},t)\,\dot{\boldsymbol{\mu}})
= 0.
\end{equation*}

If we assume an Hamiltonian system, i.e.
\begin{equation*}
  \dot{\boldsymbol{\mu}}=(\dot{x},\dot{p}) = (
  \frac{\partial H}{\partial p},-\frac{\partial H}{\partial
    x})
\end{equation*}

show that this can further be simplified to
\begin{equation*}
\frac{\partial}{\partial
  t}f(\boldsymbol{\mu},t)\,+\dot{\boldsymbol{\mu}}\cdot
\boldsymbol{\nabla_\mu}(f(\boldsymbol{\mu},t)) =
0.
\end{equation*}
Compare this to the collisionless Boltzmann equation.

{\bf 4) Vlasov equation from the BBGKY hierarchy}

We will rederive the Vlasov equation but this time in $\Gamma$-space
with the possibility to find collision corrections. This will lead to
what is called the BBGKY hierachy. The derivation is a bit long but it
is worth it because it will train our understanding the kinetic
description of a gaz! The goal is to derive the evolution equation of
the PDF $f=f(\br,\bv,t)$, that is the probability of finding a
particle at $(\br,\bv)$ in phase space.

\begin{itemize}
\item Discuss why in $\Gamma$-space, the system is determined by the
  $N$-particle (or $N$-point) distribution function
  \begin{equation*}
    f^N=f^N(\br_1,\cdots,\br_N,\bv_1,\cdots,\bv_N,t)
  \end{equation*}
\item Discuss why the one-point PDF is
  \begin{equation*}
    f^1(\br_1,\bv_1,t)=\int f^N d^3r_2\cdots d^3r_Nd^3v_2\cdots d^3v_N
  \end{equation*}
\item Discuss why the Liouville equation describes the evolution of
  $f^N$:
  \begin{equation*}
    \frac{\partial f^N}{\partial t} + \bv_\Gamma\cdot\nabla_\Gamma f^N
    = 0, \quad \bv_\Gamma = (\dot \br_1,\cdots,\dot \br_N,\dot
    \bv_1,\cdots,\dot \bv_N)
  \end{equation*}
\item Derive from Liouville's equation an evolution equation of $f^1$
  by integration over $d^3r_2\cdots d^3r_Nd^3v_2\cdots d^3v_N$. Assume
  that
  \begin{equation*}
    \dot \bv_i = \ba_i = \sum_{j=1,j\ne i}^N \ba_{ij}(\br_i,\br_j)
  \end{equation*}
  as it is for an electrostatic force
  $\ba_{ij}(\br_i,\br_j)=e/m_e\nabla_i \Phi_{ij}$,
  $\Phi_{ij}=-e/(4\pi\epsilon_0 |\br_i - \br_j|)$. Hint: Split sums
  $\sum_{j=1,j\ne i}^N$ into two parts $\sum_{j=1}^1 + \sum_{j=2}^N$
  and use integration by parts for the latter.
\item You should finally arrive at
  \begin{equation*}
    \frac{\partial f^1}{\partial t} + (\bv_1\cdot \nabla_1)f^1 +
    (N-1)\int a_{12}\nabla_{v_1}f^2 d^3r_2d^3v_2=0.
  \end{equation*}
  which involves the two-point PDF $f^2$. This leads to the BBGKY
  hierarchy of equations that is not closes.

\item Derive now from this the Vlasov equation by develloping
  \begin{equation*}
    f^2(\br_1,\br_2,\bv_1,\bv_2)=f^1(\br_1,\bv_1)\,f^1(\br_2,\bv_2) +
    g_{12}((\br_1,\br_2,\bv_1,\bv_2))
  \end{equation*}
  where $g_{12}$ is a correlation function. Why can we do this?
\item Use this ansatz and perform the integration over $d^3r_2d^3v_2$
  to recover the Vlasov equation.
\item How could we incorporate collisions in our kinetic description
  using the BBGKY hierarchy?
\end{itemize}

\end{document}

